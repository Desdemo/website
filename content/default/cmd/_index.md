---
date: "2019-06-17T09:54:00+08:00"
title: "CMD"
weight: 10
toc: false
draft: false
goimport: "xorm.io/cmd git https://gitea.com/xorm/cmd"
gosource: "xorm.io/cmd https://gitea.com/xorm/cmd https://gitea.com/xorm/cmd/tree/master{/dir} https://gitea.com/xorm/cmd/blob/master{/dir}/{file}#L{line}"
---

# CMD - Command line tools for database operation written by Go

This is the URL of the import path for [CMD](http://gitea.com/xorm/cmd).
